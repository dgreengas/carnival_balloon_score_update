var express = require('express');
var router = express.Router();
const pg = require('pg');

/**
 * TODO(developer): specify SQL connection details
 */
const connectionName =
  process.env.INSTANCE_CONNECTION_NAME || '<YOUR INSTANCE CONNECTION NAME>';
const dbUser = process.env.SQL_USER || '<YOUR DB USER>';
const dbPassword = process.env.SQL_PASSWORD || '<YOUR DB PASSWORD>';
const dbName = process.env.SQL_NAME || '<YOUR DB NAME>';

const pgConfig = {
  max: 1,
  user: dbUser,
  password: dbPassword,
  database: dbName,
};

if (process.env.NODE_ENV === 'production') {
  pgConfig.host = `/cloudsql/${connectionName}`;
}

// Connection pools reuse connections between invocations,
// and handle dropped or expired connections automatically.
let pgPool;

/* POST balloon score. */
router.post('/', function(req, res, next) {
  if (!pgPool) {
        pgPool = new pg.Pool(pgConfig);
  }
  pgPool.query('insert into balloon_scores (gameid, winner, duration) VALUES($1, $2, $3) RETURNING *', 
     [ req.body.gameid, req.body.winner, req.body.duration ],
     (err, result) => {
       if (err) {
        res.status(500).send(err)
      
       } else {
         res.send(JSON.stringify(results)) 
       }
     });
  //console.info(req.body);
  //res.send(req.body.timestamp);
});

router.post('/controller-click', function(req, res, next) {
    if (!pgPool) {
        pgPool = new pg.Pool(pgConfig);
  }
  pgPool.query('insert into balloon_controller (gameid, player, buttonid) VALUES($1, $2, $3) RETURNING *', 
     [ req.body.gameid, req.body.player, req.body.buttonid ],
     (err, result) => {
       if (err) {
        res.status(500).send(err)
      
       } else {
         res.send(JSON.stringify(results)) 
       }
     })
})

module.exports = router;