CREATE TABLE IF NOT EXISTS balloon_scores(
    id integer PRIMARY KEY DEFAULT nextval('serial'),
    gameid varchar(64),
    winner varchar(30),
    duration numeric(5,2),
    game_time timestamp current_timestamp
);

CREATE INDEX IF NOT EXISTS ON balloon_scores (winner);

CREATE TABLE IF NOT EXISTS balloon_controller (
    id integer PRIMARY KEY nextval('serial'),
    gameid varchar(64),
    player varchar(30),
    buttonid integer,
    click_time timestamp current_timestamp
);