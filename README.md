# Carnival Balloon Score Update
## Purpose
The purpose of this project is to act as the Google Cloud Function to update a POSTGRESql database. It will be called from the actual game code when a game is won and when a controller button is pressed.
## Use Cases
The data in the tables can be used for analytics of winners, presses to win, opponents presses, etc.